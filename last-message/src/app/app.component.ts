import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

// Import rxjs map operator
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app works!';

  API = 'http://localhost:3000';

  message: Object = [];
  messageAdded: boolean = false;
  newMessage :string = '';

  constructor(private http: Http) {}

  ngOnInit() {
    this.getAMessage();
  }

  addMessage() {
    if(this.newMessage.length > 3)
    {
      let send = this.newMessage;
      this.http.post(`${this.API}/message`, { send })
        .map(res => res.json())
        .subscribe(() => {
          this.showMessage();
        });
    }
  }

  showMessage() {
    this.messageAdded = true;
    this.removeMessage( this.message );
  }

  getAMessage() {
    this.http.get(`${this.API}/message`)
      .map(res => res.json())
      .subscribe(message => {
        this.message = message;
      })
  }

  removeMessage( message ) {
    this.http.delete(`${this.API}/message/${message._id}`)
      .map(res => res.json())
      .subscribe(data => {

      })
  }

  startAgain() {
    location.reload();
  }

}
