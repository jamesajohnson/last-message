import { LastMessagePage } from './app.po';

describe('last-message App', function() {
  let page: LastMessagePage;

  beforeEach(() => {
    page = new LastMessagePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
