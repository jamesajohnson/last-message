// Import dependencies
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

// MongoDB URL from the docker-compose file
const dbHost = 'mongodb://database/mean-docker';

// Connect to mongodb
mongoose.connect(dbHost);

// create mongoose schema
const messageSchema = new mongoose.Schema({
  name: String,
  age: Number
});

// create mongoose model
const Message = mongoose.model('message', messageSchema);

/* GET api listing. */
router.get('/', (req, res) => {
        res.send('api works');
});

// get messages
router.get('/messages', (req, res) => {
    Message.find({}, (err, users) => {
        if (err) res.status(500).send(error)

        res.status(200).json(users);
    });
});

// get one message
router.get('/message', (req, res) => {
    Message.findOne({}, (err, users) => {
        if (err) res.status(500).send(error)

        res.status(200).json(users);
    });
});

//delete a message by id
router.delete('/message/:id', (req, res) => {
    var id = mongoose.Types.ObjectId(req.params.id);
    Message.remove({'_id': id}, (err, users) => {
        if (err) res.status(500).send(error)

        res.status(202).json(users);
    });
});

//post a message
router.post('/message', (req, res) => {
    let message = new Message({
        name: req.body.message
    });

    message.save(error => {
        if (error) res.status(500).send(error);

        res.status(201).json({
            message: 'Message created successfully'
        });
    });
});

module.exports = router;
